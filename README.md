sclib
=====

This is a library for implementing a client for the softwarechallenge competition in Haskell. It provides the basic functionality to communicate with the server and run strategies in real-time.
