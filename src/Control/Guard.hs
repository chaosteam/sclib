{-# LANGUAGE ConstraintKinds #-}
-- | Control the execution of monadic actions with types.
--
-- This module provides a simple way to let the executed monadic actions depend on the return
-- type of a function. Here is an example:
--
-- > test :: (Guard f, Guard g, Guard h) => IO (f Int, g Int, h Int)
-- > test = do
-- >  a <- guarded (putStr "A: " >> readLn)
-- >  b <- guarded (putStr "B: " >> readLn)
-- >  c <- guarded (putStr "C: " >> readLn)
-- >  a2 <- guarded (putStr "A: " >> readLn)
-- >  return (liftA2 (+) a a2, b, c)
--
-- @h@, @f@ and @g@ are the guard types in this case. Now, if we only wish to compute @b@ and @c@, we
-- can do this in a way that the user doesn't even need to input the values needed to compute a!
-- We just use 'Proxy' as guard type for @f@:
--
-- > test >>= (\(Proxy, Identity b, Identity c) -> print b >> print c)
--
-- The `Identity` guard type is used when we don't want any special behaviour.
--
-- In another case, we might be only interested in A and B, and not C. Then we can just inhibit C:
--
-- > test >>= (\(Identity a, Identity b, Proxy) -> print a >> print b)
--
-- Through guards, we only ever execute the required monadic actions. In the last example, the user is never asked
-- to input C, because we don't need it.
module Control.Guard
  ( Guard
  , guarded
  -- * Reexports
  , Proxy(..)
  , Identity(..)
  ) where

import Control.Applicative
import Data.Functor.Identity
import Data.Proxy
import Prelude

import qualified Data.Traversable as T

-- | A guard is a type that can control the execution of monadic or applicative actions.
type Guard f = (Applicative f, T.Traversable f)

-- | Guard a computation with a guard type.
guarded :: (Guard f, Applicative m) => m a -> m (f a)
guarded = T.sequenceA . pure
-- Note: even though fmap pure would also typecheck, it's behaviour is different
-- (it would always execute the action, no matter what f)
