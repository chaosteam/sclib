{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}
module SC.Protocol
  ( module SC.Protocol
  , module SC.LaxXML
  ) where

import Control.Applicative
import Control.Monad
import Control.Monad.Loops
import Control.Monad.Trans
import Data.Conduit
import Data.Maybe
import Data.XML.Types
import Prelude
import System.CPUTime
import Text.Printf

import qualified Data.Text as T

import Control.Guard
import SC.GameDefinition
import SC.LaxXML
import SC.Types

type Room = T.Text

--------------------------------------------------------------------------------
parseWelcome :: Parser' (Room, PlayerColor)
parseWelcome = do
  r <- tag "joined" $ textAttribute "roomId"
  c <- parent "room" ("roomId" =?= r) $
    tag "data" $ untilJust $ f <$> textAttribute "class" <*> attribute "color" readPlayerColorL
  return (r,c)
 where f m | m == "welcome" || m == "welcomeMessage" = Just
           | otherwise = const Nothing

parseGameResult :: Parser' GameResult
parseGameResult = parentName "condition" $ GameResult
  <$> lookahead (maybeEOF . parentName "winner" $ readWinner <$> content)
  <*> parentName "reason" content
  where readWinner "RED"  = PlayerRed
        readWinner "BLUE" = PlayerBlue
        readWinner _      = error "Invalid value for winner tag of GameResult"

-- | Parse a message from a given room.
inRoom :: Monad m => Room -> Parser o m a -> Parser o m a
inRoom r = parent "room" ("roomId" =?= r)

-- | Parse a "state" message, which is sent as part of a memento or stored in a replay file.
--
-- Returns the TurnState and the player who has the next turn
-- the init state (only if requested by the guard), the default move for this turn,
-- the last enemy move (Nothing if not available, such as for the first turn) and
-- the new game state. Additionaly, it returns the player who has to move next.
--
-- If the game has ended the function also returns the game result.
parseState :: (Guard f, Guard g) => MementoParser init move state
           -> Parser o IO (Maybe GameResult, TurnState f g init move state, PlayerColor)
parseState (MementoParser extraAttrs parseBody) = element "state" attrs $
  \(extra,cur) -> do
    body <- parseBody extra cur
    result <- maybeEOF parseGameResult
    return (result, body, cur)
 where
  attrs = (,) <$> extraAttrs <*> attribute "currentPlayer" readPlayerColorL

-- | Parse a "memento" message, which is send by the server after each turn and contains
-- the updated game state.
parseMemento :: (Guard f, Guard g)
             => MementoParser init move state
             -> Parser o IO (Either GameResult (TurnState f g init move state, PlayerColor))
parseMemento p =
  handleGameEnd $ parent "data" ("class" =?= "memento") $ do
    start <- liftIO getCPUTime

    liftIO $ putStrLn "Parsing turn state ..."
    (result, body, cur) <- parseState p

    end <- liftIO getCPUTime
    void $ liftIO $ printf "Took %.3f seconds to parse enemy response.\n" $ fromInteger (end - start) / (10^(12 :: Int) :: Double)
    return $ maybe (Right (body,cur)) Left result
 where
  handleGameEnd = fmap (fromMaybe $ Left defaultGameResult) . maybeEOF
  defaultGameResult = GameResult Nothing $ T.pack "The game ended, but the server did not send the game result."

waitForMoveRequest :: Room -> Parser' ()
waitForMoveRequest r = inRoom r $ tag "data" $ "class" =?= "sc.framework.plugins.protocol.MoveRequest"

waitForProtocolStart :: Monad m => Parser o m ()
waitForProtocolStart = tagOpen "protocol" (return ())

playerNameAttribute :: AttributeParser String
playerNameAttribute = T.unpack <$> textAttribute "displayName"

parsePlayerData :: AttributeParser a -> Parser' (PlayerData a)
parsePlayerData p = PlayerData <$> tag "red" p <*> tag "blue" p

--------------------------------------------------------------------------------
sendElement :: Monad m => Name -> [(Name, T.Text)] -> Conduit i m Event -> Conduit i m Event
sendElement name attributes body = do
  yield $ EventBeginElement name $ map (\(n,c) -> (n, [ContentText c])) attributes
  body
  yield $ EventEndElement name

sendTag :: Monad m => Name -> [(Name, T.Text)] -> Producer m Event
sendTag name attributes = sendElement name attributes $ return ()

toRoom :: Monad m => Room -> Conduit i m Event -> Conduit i m Event
toRoom r = sendElement "room" [("roomId", r)]

sendJoin :: Monad m
         => T.Text      -- ^ The game type (ex.: @swc_2014_sixpack)
         -> Maybe Room  -- ^ Reservation code (Nothing to join any game)
         -> Producer m Event
sendJoin game Nothing  = sendTag "join" [("gameType", game)]
sendJoin _ (Just c) = sendTag "joinPrepared" [("reservationCode", c)]

sendProtocolStart :: Monad m => Producer m Event
sendProtocolStart = yield $ EventBeginElement "protocol" []

--------------------------------------------------------------------------------
showPlayerColor :: PlayerColor -> T.Text
showPlayerColor PlayerRed = "RED"
showPlayerColor PlayerBlue = "BLUE"

readPlayerColor :: T.Text -> Maybe PlayerColor
readPlayerColor "RED" = Just PlayerRed
readPlayerColor "BLUE" = Just PlayerBlue
readPlayerColor _ = Nothing

readPlayerColorL :: T.Text -> Maybe PlayerColor
readPlayerColorL = readPlayerColor . T.toUpper
