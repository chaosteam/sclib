{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE ExistentialQuantification #-}
module SC.GameDefinition
  ( GameDefinition(..)
  , MementoParser(..)
  , TurnState(..)
  , module Control.Guard
  , module Data.XML.Types
  , Producer()
  ) where

import Control.Lens
import Data.Conduit
import Data.Text (Text)
import Data.XML.Types

import Control.Guard
import SC.LaxXML
import SC.Types

-- | A 'GameDefinition' contains all the game-specific functions.
--
-- It's parametrized on 3 types:
--
--   * init: Additional information that a strategy only needs at start (ex.: board)
--   * move: Type of the move that is produced by the strategy
--   * state: State that needs to be passed to the strategy after each move
data GameDefinition init move state = GameDefinition
  { -- | Name of the game, as understood by the server. Example: @swc_2014_sixpack@.
    gameType  :: Text

    -- | The memento parser. It receives our own player color as an argument.
  , mementoParser :: PlayerColor -> MementoParser init move state

    -- | Send a move.
  , sendMove :: forall m. Monad m => move -> Producer m Event
  }

-- | Definition of game state parser.
data MementoParser init move state = forall x. MementoParser
  { -- | Attributes of the "state" tag, passed as argument to the parser for the body.
    mementoAttributes :: AttributeParser x

    -- | The parser for the body of the state tag (inside a memento tag).
    -- It receives the parsed attributes of the state tag and the color of
    -- the next player as an argument.
    -- The parser should be able to optionally do a full parse of initial state
    -- required for the strategy to start up (of type @init@).
    -- Additionally, it returns the default move for this turn, the last enemy move (Nothing if that
    -- is not available) and the new game state.
  , mementoBody      :: forall f g o. (Guard f, Guard g) => x -> PlayerColor -> Parser o IO (TurnState f g init move state)
  }

instance Functor (MementoParser init def) where
  fmap f (MementoParser attr body) = MementoParser attr (body & mapped.mapped.mapped.mapped %~ f)

-- | The part of the game state that is necessary for each turn.
data TurnState f g init move state = TurnState
  { initState :: f init
  , defaultMove :: move
  , lastMove :: g move
  , gameState :: state
  } deriving (Functor, Show, Eq)
