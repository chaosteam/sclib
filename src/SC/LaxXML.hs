{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE OverloadedStrings #-}

-- | This module implements a lax xml parser that skips mismatching elements
-- on top of the strict parser provided by xml-conduit.
module SC.LaxXML
  ( -- * Structure parsers
    Parser
  , Parser'
  , element
  , tag
  , tagOpen
  , parent
  , parentName
  , untilEOF
  , maybeEOF
  , lookahead
  , content
    -- * Attribute parsers
  , AttributeParser
  , attribute
  , optionalAttribute
  , textAttribute
  , readAttribute
  , attributeValue
  , (=?=)
  ) where

import Control.Error
import Control.Monad
import Control.Monad.Free
import Control.Monad.Morph
import Control.Monad.Trans.State.Strict
import Data.Conduit
import Data.Conduit.Lift (runStateC, execStateC)
import Data.Functor
import Data.XML.Types
import Prelude

import qualified Data.Text as T

import SC.Error

{- $setup
>>> import Text.XML.Stream.Parse hiding (content)
>>> import qualified Data.Conduit.List as CL
>>> import Data.Conduit
>>> import Control.Monad.Trans.Except
>>> import Data.Functor.Identity
>>> import qualified Data.Text as T
>>> import Data.Monoid
>>> :set -XOverloadedStrings
-}

-- | A parser is just a consumer of XML events.
-- There is an ExceptT layered on top of the conduit, which allows to report errors.
-- The ExceptT guarrantes that parsing stops after the first error.
-- The type arguments of this synonym, o for the output of the conduit and m for the underlying monad, will most often be polymorphic.
-- If you want to define a top-level parser, you can probably use 'Parser''.
type Parser o m = ExceptT Error (ConduitM Event o m)

-- | Many top-level parses can be polymorphic in the monad and output type of the conduit.
-- This type synonym is like 'Parser', but for any monad and output type.
type Parser' a = forall o m. Monad m => Parser o m a

-- | The type of the attributes of a XML element.
type Attributes = [(Name, [Content])]

-- | @element n attributes f@ is a consumer that tries to parse an element with the name @n@ and where the attribute parser @attributes@ succeeds.
-- The return value of the attribute parser will be feed to @f@, which is executed to parse the body of the xml element.
-- @f@ only receives the events that are generated between the opening and the closing tag of the parsed element.
--
-- This parser will skip all elements that do not match.
--
-- Examples:
--
-- Mismatching elements are skipped:
--
-- >>> parseLBS def "<foo></foo><bar prefix='baz'>content</bar>" $$ runExceptT (element "bar" (textAttribute "prefix") (\pref -> fmap (pref <>) content))
-- Right "bazcontent"
--
-- The inner parser only sees the contents of the enclosing element, so it will see EOF when it hits the closing </a> tag:
--
-- >>> parseLBS def "<a><b>1</b><b>2</b></a>" $$ runExceptT (element "a" (return ()) (\() -> untilEOF $ element "b" (return ()) $ \() -> content))
-- Right ["1","2"]
--
-- When no tag matching the criteria is found, EOF is thrown:
--
-- >>> parseLBS def "<a></a><b></b>" $$ runExceptT (element "c" (return ()) (\() -> content))
-- Left EOF
--
-- If the xml is malformed, then an exception is thrown:
--
-- >>> parseLBS def "<a></a><b></b<" $$ runExceptT (element "c" (return ()) (\() -> content))
-- *** Exception: ParseError {errorContexts = [], errorMessage = "Failed reading: takeWhile1", errorPosition = 1:11}
--
element :: Monad m => Name -> AttributeParser a -> (a -> Parser o m b) -> Parser o m b
element n attributeParser f = do
  as <- findStart n
  case runAttributeParser attributeParser as of
    Nothing        -> findEnd n >> element n attributeParser f
    Just (Left e)  -> findEnd n >> throwE e
    Just (Right a) -> do
      r <- lift $ go 0 =$= runExceptT (f a)
      case r of
        Left EOF -> findEnd n >> element n attributeParser f
        Left e   -> throwE e
        Right b  -> b <$ findEnd n

  where go :: Monad m => Int -> Conduit Event m Event
        go !deep = do
          x <- await
          case x of
            Nothing -> return ()
            Just x' -> do
              let deep' = case x' of
                    EventEndElement n'     | n' == n -> deep - 1
                    EventBeginElement n' _ | n' == n -> deep + 1
                    _ -> deep
              if deep' < 0 then leftover x' else yield x' >> go deep'

-- | Skip forward till an opening tag with the given name is found. Return the attributes
-- of that tag.
findStart :: Name -> Parser' Attributes
findStart n = go where
  go = do
    x <- lift await
    case x of
      Nothing -> throwE EOF
      Just (EventBeginElement n' as) | n' == n -> return as
      _ -> go
{-# INLINE findStart #-}

-- | Skip forward till a closing tag with the given name is found.
findEnd :: Name -> Parser' ()
findEnd n = go 0 where
  go :: Int -> Parser' ()
  go !deep = do
    x <- lift await
    case x of
      Nothing -> throwE EOF
      Just (EventBeginElement n' _) | n' == n -> go $ succ deep
      Just (EventEndElement n')     | n' == n -> when (deep /= 0) (go $ pred deep)
      _ -> go deep
{-# INLINE findEnd #-}

-- | Only parse an opening tag.
tagOpen :: Name -> AttributeParser a -> Parser' a
tagOpen n attributeParser = do
  as <- findStart n
  case runAttributeParser attributeParser as of
    Nothing -> findEnd n >> tagOpen n attributeParser
    Just (Left e) -> findEnd n >> throwE e
    Just (Right a) -> return a

-- | This is a shorthand for @element n p return@.
-- This often happens when an element contains no contents.
tag :: Name -> AttributeParser a -> Parser' a
tag n p = element n p return

-- | This is a shorthand for @element n o . const@.
-- Use this when you don't care about the contents of the attributes of an element.
parent :: Monad m => Name -> AttributeParser a -> Parser o m b -> Parser o m b
parent n p = element n p . const

-- | This is like @parent@, but without a parser for attributes.
-- The attribute parser it uses will always succeed, for any set of attributes.
parentName :: Monad m => Name -> Parser o m a -> Parser o m a
parentName n = parent n (return ())

-- | Run a parser, but use `leftover` to push back the consumed input. This means
-- that further parsers in the current monadic binding will consume the input again.
--
-- Note: This function needs to store the consumed input, so memory usage is propertional
-- to the number of tags consumed or skipped by the supplied parser.
lookahead :: Monad m => Parser o m a -> Parser o m a
lookahead p = do
  (r, consumed) <- lift $ runStateC [] $
    awaitForever (\x -> lift (modify (x:)) >> yield x) =$= transPipe lift (runExceptT p)
  ExceptT $ r <$ mapM_ leftover consumed

-- | Parse the text content of an xml element.
content :: Parser' T.Text
content = lift . fmap (contentsToText . reverse) . execStateC [] . awaitForever $
 \x -> case x of
  EventContent t -> lift $ modify (t :)
  EventCDATA   t -> lift $ modify (ContentText t :)
  _              -> return ()

-- | The functor from which the free monad AttributeParser is generated.
data AttributeParserF a = NeedAttribute Name (T.Text -> Maybe a)
                        | OptionalAttribute Name (Maybe T.Text -> Maybe a)
                        | AttributeEqual Name T.Text a deriving Functor

-- | An AttributeParser is used to parse the attributes of an xml element. It has two notions of failure:
--      * It can fail and skip the element
--      * It can fail with an error
type AttributeParser = Free AttributeParserF

-- | Parse a single attribute with the given name.
-- This will throw an error if the attribute doesn't exist or parsing fails.
--
-- Examples:
--
-- >>> parseLBS def "<a val='3'></a>" $$ runExceptT (element "a" (attribute "val" (\x -> readMay (T.unpack x) :: Maybe Int)) $ \i -> return $ T.pack $ show i)
-- Right "3"
--
-- >>> parseLBS def "<a>content</a>" $$ runExceptT (element "a" (attribute "val" return) (\val -> fmap (val <>) $ content))
-- Left (MissingAttribute (Name {nameLocalName = "val", nameNamespace = Nothing, namePrefix = Nothing}))
--
-- >>> parseLBS def "<a val='notANumber'></a>" $$ runExceptT (element "a" (attribute "val" (\x -> readMay (T.unpack x) :: Maybe Int)) $ \i -> return $ T.pack $ show i)
-- Left (AttributeParseFailed (Name {nameLocalName = "val", nameNamespace = Nothing, namePrefix = Nothing}) "notANumber")
attribute :: Name -> (T.Text -> Maybe a) -> AttributeParser a
attribute = (liftF .) . NeedAttribute

-- | Like 'attribute', but does not fail if the attribute is not present.
--
-- Also, if the attribute couldn't be parsed successfully, this will skip the
-- tag instead of failing with an error.
optionalAttribute :: Name -> (Maybe T.Text -> Maybe a) -> AttributeParser a
optionalAttribute = (liftF .) . OptionalAttribute

-- | This is a specialization of attribute for text attributes.
textAttribute :: Name -> AttributeParser T.Text
textAttribute n = attribute n return

-- | Uses @read@ as the parsing function for @attribute@.
readAttribute :: Read a => Name -> AttributeParser a
readAttribute n = attribute n $ readMay . T.unpack

-- | Assert that a attribute must have a specified value.
-- The parser will skip the element if the attribute has another value.
--
-- Example:
--
-- >>> parseLBS def "<a val='false'></a><a val='true'>content</a>" $$ runExceptT (element "a" (attributeValue "val" "true") $ \() -> content)
-- Right "content"
--
-- Here the first element is skipped, because the attribute @val@ isn't @true@.
attributeValue :: Name -> T.Text -> AttributeParser ()
attributeValue n v = liftF $ AttributeEqual n v ()

-- | An infix alias for attributeValue
(=?=) :: Name -> T.Text -> AttributeParser ()
(=?=) = attributeValue
infixl 7 =?=

-- | Execute a monadic action in an ExceptT until EOF is hit.
-- All values will be collected in a list.
-- If any other error is returned, then it will be passed through.
--
-- Example:
--
-- >>> CL.sourceList [1,2,3,4,5] $$ runExceptT (untilEOF $ lift await >>= maybe (throwE EOF) (\a -> if a == 4 then throwE EOF else return a))
-- Right [1,2,3]
--
-- In this example, if the number 4 is consumed, EOF is thrown. This stops the process, and all values already consumed are returned.
untilEOF :: Monad m => ExceptT Error m a -> ExceptT Error m [a]
untilEOF action = do
  x <- lift $ runExceptT action
  case x of
    Right a -> (a:) <$> untilEOF action
    Left EOF -> return []
    Left e -> throwE e

-- | @maybeEOF@ is similar to @untilEOF@, but it will only consume one element.
-- If that fails, it returns @Nothing@.
maybeEOF :: Monad m => ExceptT Error m a -> ExceptT Error m (Maybe a)
maybeEOF action = do
  x <- lift $ runExceptT action
  case x of
    Right a -> return $ Just a
    Left EOF -> return Nothing
    Left e -> throwE e

-- | This function executes an attribute parser.
-- It takes the attributes and the parser as arguments.
-- If it returns Nothing, that means one of the attribute assertions created by @attributeValue@ failed.
-- Otherwise, it returns either an Error if something else went wrong, or the value of the parser.
runAttributeParser :: AttributeParser a -> Attributes -> Maybe (Either Error a)
runAttributeParser = (runExceptT .) . evalStateT . iterM (join . gets . go)
 where
  go :: AttributeParserF (StateT Attributes (ExceptT Error Maybe) a) -> Attributes -> StateT Attributes (ExceptT Error Maybe) a
  go (NeedAttribute n f) l = do
    mv <- useAttribute n l
    case mv of
      Nothing -> lift . throwE $ MissingAttribute n
      Just v  -> fromMaybe (lift . throwE $ AttributeParseFailed n v) . f $ v
  go (OptionalAttribute n f) l = do
    mv <- useAttribute n l
    fromMaybe (lift $ lift Nothing) . f $ mv
  go (AttributeEqual n v a) l = do
    mv <- useAttribute n l
    case mv of
      Just v' | v' == v -> a
      _ -> lift $ lift Nothing

  useAttribute n l = do
    let (mv, l') = lookupDelete n id l
    put l'
    pure $ contentsToText <$> mv

  lookupDelete _ front [] = (Nothing, front [])
  lookupDelete k front (a@(k',v):as)
    | k == k' = (Just v, front as)
    | otherwise = lookupDelete k (front . (a:)) as

contentsToText :: [Content] -> T.Text
contentsToText = T.concat . map toText
  where toText (ContentText t) = t
        toText (ContentEntity e) = T.concat ["&", e, ";"]
