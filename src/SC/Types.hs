{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module SC.Types
  ( GameConfiguration(..), enemyColor
  , GameResult(..)
  , PlayerData(..), self, enemy, red, blue, startPlayer, playerWithColor
  , PlayerColor(..), oppositePlayerColor
  ) where

import Control.Applicative
import Control.DeepSeq.Generics
import Control.Lens
import Data.Semigroup
import Data.Serialize
import Data.Typeable
import GHC.Generics
import Instances.TH.Lift()
import Language.Haskell.TH.Lift
import Prelude

import qualified Data.Foldable as F
import qualified Data.Text as T

-- | There are two player colors, red and blue.
data PlayerColor = PlayerRed | PlayerBlue
  deriving (Eq, Ord, Show, Enum, Generic, Typeable)
deriveLift ''PlayerColor
instance NFData PlayerColor where rnf = genericRnf
instance Serialize PlayerColor

-- | The opposite player color is the color of the enemy.
oppositePlayerColor :: PlayerColor -> PlayerColor
oppositePlayerColor PlayerRed = PlayerBlue
oppositePlayerColor PlayerBlue = PlayerRed

-- | The configuration of the game stays constant for the whole game
-- and is determined before the game begins.
data GameConfiguration = GameConfiguration
  { selfColor  :: !PlayerColor
  , startColor :: !PlayerColor
  } deriving (Show, Generic, Typeable)
instance Serialize GameConfiguration

-- | Get the color of the enemy.
enemyColor :: GameConfiguration -> PlayerColor
enemyColor = oppositePlayerColor . selfColor

-- | The result of the game, as reported by the server.
data GameResult = GameResult
  { winner :: Maybe PlayerColor -- ^ Can be Nothing if the game ended in a tie.
  , reason :: T.Text
  } deriving (Eq, Show, Typeable)

--------------------------------------------------------------------------------
-- | Store some state for each player.
data PlayerData s = PlayerData
  { _red  :: !s
  , _blue :: !s
  } deriving (Functor, F.Foldable, Traversable, Eq, Ord, Show, Generic, Typeable)
makeLenses ''PlayerData
deriveLift ''PlayerData
instance NFData s => NFData (PlayerData s) where rnf = genericRnf
instance Serialize s => Serialize (PlayerData s)

instance Semigroup s => Semigroup (PlayerData s) where
  PlayerData a b <> PlayerData a' b' = PlayerData (a <> a') (b <> b')

instance Monoid s => Monoid (PlayerData s) where
  mempty = PlayerData mempty mempty
  mappend (PlayerData a b) (PlayerData a' b') = PlayerData (mappend a a') (mappend b b')

instance Applicative PlayerData where
  pure x = PlayerData x x
  PlayerData fa fb <*> PlayerData a b = PlayerData (fa a) (fb b)

instance Each (PlayerData a) (PlayerData b) a b where each = traverse

playerWithColor :: PlayerColor -> Lens' (PlayerData s) s
playerWithColor PlayerRed = red
playerWithColor PlayerBlue = blue
{-# INLINE playerWithColor #-}

-- | Get a lens into PlayerData for our own state.
self :: GameConfiguration -> Lens' (PlayerData s) s
self = playerWithColor . selfColor
{-# INLINE self #-}

-- | Get a lens into PlayerData for the opponents state.
enemy :: GameConfiguration -> Lens' (PlayerData s) s
enemy = playerWithColor . enemyColor
{-# INLINE enemy #-}

-- | Get a lens into PlayerData for the start player's state.
startPlayer :: GameConfiguration -> Lens' (PlayerData s) s
startPlayer = playerWithColor . startColor
{-# INLINE startPlayer #-}
