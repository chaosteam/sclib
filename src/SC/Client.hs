{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}
module SC.Client
  ( client
  ) where

import Control.Applicative
import Control.Error hiding ((??))
import Control.Lens hiding (strict)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Trans.Resource
import Data.Conduit
import Data.Foldable (traverse_, for_)
import Data.Monoid
import Data.Serialize
import Data.Void
import Data.XML.Types
import Foreign.Storable
import Prelude
import System.Mem
import System.Time.Monotonic

import qualified Data.Text as T
import qualified Data.Text.IO as T

import Control.Guard
import SC.Error
import SC.GameDefinition hiding (defaultMove)
import SC.LaxXML
import SC.Protocol
import SC.Run
import SC.Types

-- | This monad allows to parse incoming protocol messages and also to send
-- replies. It also allows has a ResourceT layer which allows to manage resources
-- and guarantee their cleanup.
type ProtocolM = Parser Event (ResourceT IO)

-- | This data type holds internal information used for the implementation of the
-- client monad.
data ClientState m s = ClientState
  { -- | The current strategy process. This should be set to Nothing if the
    -- strategy needs to be restarted.
    strategyProcess :: Maybe (ReleaseKey, StrategyProcess m s)

    -- | The player who has the current turn. This should only
    -- be Nothing at the start when we haven't yet parsed any
    -- game states.
  , currentPlayerColor :: Maybe PlayerColor
  }

-- | Internal configuration of the client monad.
data ClientConfig init move state = ClientConfig
  { -- | The room of the game
    gameRoom :: T.Text

    -- | Configuration of the game
  , gameConfiguration :: GameConfiguration

    -- | Definition of the game
  , gameDefinition :: GameDefinition init move state

    -- | The strategy to use
  , strategyName :: String

    -- | True to enable strict mode (this means that the client will exit if the
    -- strategy crashes instead of restarting it).
  , strictMode :: Bool
  }

-- | The client monad is used to implement a software challenge client.
type ClientM i m s a =
  (Serialize i, Storable m, Storable s) =>
  StateT (ClientState m s) (ReaderT (ClientConfig i m s) (ExceptT GameResult ProtocolM)) a

-- | Run the client.
client :: (Serialize init, Storable move, Storable state)
       => GameDefinition init move state -- ^ Definition for the game to play
       -> String       -- ^ Name of the strategy to use
       -> Maybe T.Text -- ^ Reservation code (Nothing to join any game)
       -> Bool         -- ^ False to restart strategy after crash. Otherwise, we exit.
       -> ProtocolM GameResult
client gameDefinition strategyName res strictMode = do
  -- Join the game
  liftIO $ startBlock "Join"
  lift $ sendProtocolStart >> sendJoin (gameType gameDefinition) res
  liftIO $ putStrLn "Sent join game message. Awaiting begin of server response ..."
  waitForProtocolStart
  liftIO $ putStrLn "Got protocol start from server."

  -- Parse the welcome message
  (gameRoom, selfPlayerColor) <- parseWelcome
  liftIO $ T.putStrLn $ "Assigned color " <> showPlayerColor selfPlayerColor
  liftIO endBlock

  let gameConfiguration = GameConfiguration selfPlayerColor PlayerRed
      initState         = ClientState Nothing Nothing
  exceptT return absurd $ runReaderT (evalStateT mainLoop initState) ClientConfig{..}

data MoveInfo m s = MoveInfo
  { key :: ReleaseKey
  , sp  :: StrategyProcess m s
  , defaultMove :: m
  }

-- | Handle the memento message. This will deal with restarting the strategy process
-- if needed, apply the enemy move and update the game state if required.
-- It returns Just MoveInfo if we need to do the next move, Nothing otherwise.
handleMemento :: Bool -> ClientM i m s (Maybe (MoveInfo m s))
handleMemento isStart = StateT $ \s@ClientState{..} -> case strategyProcess of
  Just (key, p) -> do
    ownColor <- asks (selfColor . gameConfiguration)
    (TurnState Proxy defMove enemyMove gs, n) <- parse
    liftIO $ do
      when (maybe False (ownColor /=) currentPlayerColor) $
        for_ enemyMove $ tellEnemyMove p
      tellGameState p gs
      return ( MoveInfo key p defMove <$ guard (n == ownColor)
             , s { currentPlayerColor = Just n }
             )
  Nothing -> do
    ownColor <- asks (selfColor . gameConfiguration)
    (TurnState (Identity initState) defMove _ gs, n) <- parse
    ClientConfig{strategyName,gameConfiguration} <- ask
    (key, p) <- lift . lift . lift . flip allocate killStrategy $
      createStrategy strategyName gameConfiguration gs initState
    return ( MoveInfo key p defMove <$ guard (n == ownColor)
           , ClientState (Just (key,p)) (Just n)
           )
  where parse = do
          ClientConfig{gameConfiguration, gameDefinition, gameRoom} <- ask
          lift . ExceptT . mapExceptT (transPipe lift) . inRoom gameRoom $
            parseTurnState $ mementoParser gameDefinition $ selfColor gameConfiguration
        parseTurnState p
          | isStart = parseMemento p   & mapped.mapped._1 %~ noEnemyMove
          | otherwise = parseMemento p & mapped.mapped._1 %~ justEnemyMove
        noEnemyMove t@TurnState{lastMove = Proxy} = t { lastMove = Nothing }
        justEnemyMove t@TurnState{lastMove = Identity m} = t { lastMove = Just m }

-- | Run a strategy process to compute a turn and send it after the move time elapsed.
doTurn :: MoveInfo m s -> ClientM i m s ()
doTurn MoveInfo{..} = do
  !send <- asks $ sendMove . gameDefinition
  room <- asks gameRoom
  liftIO $ do
    startBlock "Do turn"
    computeStrategy sp
    putStrLn "Waiting for move request ..."

  clock <- liftIO newClock
  lift . lift . lift $ waitForMoveRequest room
  elapsed <- liftIO $ clockGetTime clock
  liftIO $ delay 1.8

  liftIO $ putStrLn "Sending move ..."
  move <- liftIO $ getStrategyResult sp
  lift . lift . lift . lift $ toRoom room . send . fromMaybe defaultMove $ move
  liftIO endBlock

  t <- liftIO $ clockGetTime clock
  liftIO $ do
    startBlock "Timing"
    putStrLn $ "Total time: " ++ show t
    putStrLn $ "Time elapsed before move request: " ++ show elapsed
    putStrLn $ "Time for move: " ++ show (t - elapsed)
    endBlock

    putStrLn "Stopping strategy ..."
    stopStrategy sp
    performGC

  when (isNothing move) $ do
     liftIO $ do
       putStrLn "## ALERT ## STRATEGY CRASHED [This is usually a big disadvantage, because all cached values are lost]"
       release key
     strict <- asks strictMode
     lift . lift . lift $ when strict . throwE $ StrictMode "Strategy crashed"
     modify $ \s -> s { strategyProcess = Nothing }

mainLoop :: ClientM i m s a
mainLoop = do
  handleMemento True >>= traverse_ doTurn
  forever $ handleMemento False >>= traverse_ doTurn

blockWidth :: Int
blockWidth = 50

startBlock :: String -> IO ()
startBlock name = putStrLn $ '\n' : header
  where header = start ++ replicate (blockWidth - length start) '-'
        start = "-- " ++ name ++ " "

endBlock :: IO ()
endBlock = putStrLn $ replicate blockWidth '-'
