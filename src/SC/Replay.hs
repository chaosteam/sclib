{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
module SC.Replay
  ( Replay(..)
  , loadReplay
  , loadReplayFromRaw
  , loadReplayFromSource
  ) where

import Control.Applicative
import Control.Lens
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Resource
import Data.ByteString (ByteString)
import Data.Conduit
import Data.Conduit.Binary
import Data.Conduit.Zlib
import Prelude
import System.Exit
import Text.XML.Stream.Parse

import SC.Error
import SC.GameDefinition
import SC.Protocol
import SC.Types

-- | A replay describes an already-played game. It may be read from a replay file, which
-- is provided by the server.
data Replay init move state = Replay
  { playerNames :: PlayerData String    -- ^ Names of the participants
  , gameResult :: GameResult            -- ^ The result of the game

    -- | The situation at the beginning of the game
  , initialSituation :: TurnState Identity Proxy init move state

    -- | A list with one entry for each turn, containing the turn state for the game
    -- after the specified move was played.
  , turns :: [TurnState Identity Identity init move state]
  } deriving (Show, Eq)

-- | Load a replay from the given file.
loadReplay :: GameDefinition init move state -> FilePath -> IO (Replay init move state)
loadReplay game path = runResourceT $ loadReplayFromSource game (sourceFile path $= ungzip)

-- | Load a replay from an already ungzip'ed XML file.
loadReplayFromRaw :: GameDefinition init move state -> FilePath -> IO (Replay init move state)
loadReplayFromRaw game = runResourceT . loadReplayFromSource game . sourceFile

-- | Load a replay from the given XML stream.
loadReplayFromSource :: forall init move state m. (MonadIO m, MonadThrow m)
                     => GameDefinition init move state
                     -> Source m ByteString
                     -> m (Replay init move state)
loadReplayFromSource GameDefinition{..} source = source $= parseBytes def $$
  errorT (liftIO . err) (mapExceptT (transPipe liftIO) parse)
 where
  parse :: Parser o IO (Replay init move state)
  parse = parentName "object-stream" $ do
    names <- lookahead $ tagOpen "state" (return ()) >> parsePlayerData playerNameAttribute
    (_, i, _) <- parseState (mementoParser PlayerRed)
    (res, ts) <- parseTurns
    return $ Replay names res i ts

  parseTurns :: Parser o IO (GameResult, [TurnState Identity Identity init move state])
  parseTurns = do
    cur <- lookahead $ tagOpen "state" $ attribute "currentPlayer" readPlayerColorL
    (r, t, _) <- parseState $ mementoParser cur
    case r of
      Just res -> return (res, [t])
      Nothing  -> fmap (t :) <$> parseTurns
  err x = putStrLn x >> exitFailure
