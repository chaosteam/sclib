{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module SC.Main
  ( defaultMain
  , module SC.GameDefinition
  , module SC.Strategy
  , module SC.Error
  , module SC.Protocol
  , module SC.Types
  ) where

import Blaze.ByteString.Builder
import Control.DeepSeq
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Resource
import Data.Conduit (($$), ($=), (=$))
import Data.Conduit.Binary
import Data.Conduit.Network
import Data.Monoid
import Data.Serialize
import Foreign.Storable
import Network.Socket (close, setSocketOption, SocketOption(NoDelay, Cork), withSocketsDo)
import Options.Applicative
import System.Exit
import System.IO
import System.IO.Error
import System.Remote.Monitoring
import Text.XML.Stream.Parse
import Text.XML.Stream.Render

import qualified Data.Conduit.List as CL
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Network.BSD as N
import qualified Network.Socket as N

import SC.Client
import SC.Error
import SC.GameDefinition
import SC.Protocol
import SC.Run
import SC.Strategy
import SC.Types
import System.IPC.Util

data ClientArgs = ClientArgs
  { host :: String
  , port :: Int
  , reservation :: Maybe T.Text
  , strategy :: Maybe String
  , strict :: Bool
  , debug :: Bool
  , ekg :: Bool
  }

clientArgs :: ParserInfo ClientArgs
clientArgs = info (helper <*> parser) $ fullDesc <> header "A bot playing the game `Hey, danke für den Fish!` created for the SoftwareChallenge 2015."
  where parser = ClientArgs
               <$> strOption (value "localhost" <> short 'h' <> long "host" <> metavar "HOST" <> help "The host on which the game server runs.")
               <*> option auto (value 13050 <> short 'p' <> long "port" <> metavar "PORT" <> help "The port of the game server to connect to.")
               <*> optional (fmap T.pack $ strOption $ short 'r' <> long "reservation" <> metavar "CODE" <> help "Use a reservation code to join a specific game.")
               <*> optional (strOption (short 's' <> long "strategy" <> metavar "STRATEGY" <> help "Use the given strategy"))
               <*> switch (long "strict" <> help "Enable strict mode. In strict mode, the client does not try to correct errors and never resets the strategy.")
               <*> switch (long "debug" <> help "Enable debug mode. This will print log received network messages to stderr.")
               <*> switch (long "ekg" <> short 'e' <> help "Enable EKG monitoring (applies onĺy to the runner).")
               <*  optional (strArgument mempty)  -- Hack: The game server gui passes "" as a last argument when executing, for whatever reason

-- | Returns the first action from a list which does not throw an exception.
-- If all the actions throw exceptions (and the list of actions is not empty),
-- the last exception is thrown.
firstSuccessful :: [IO a] -> IO a
firstSuccessful [] = error "firstSuccessful: empty list"
firstSuccessful (p:ps) = catch p $ \(e :: IOException) ->
    case ps of
        [] -> throwIO e
        _  -> firstSuccessful ps

-- | Connect to the given host and port.
connect :: String -> Int -> IO N.Socket
connect hostname portnr = do
  proto <- N.getProtocolNumber "tcp"
  let hints = N.defaultHints { N.addrFlags = [N.AI_ADDRCONFIG]
                             , N.addrProtocol = proto
                             , N.addrSocketType = N.Stream
                             }
  addrs <- N.getAddrInfo (Just hints) (Just hostname) (Just $ show portnr)
  firstSuccessful $ map tryToConnect addrs
  where
  tryToConnect addr =
    bracketOnError
      (N.socket (N.addrFamily addr) (N.addrSocketType addr) (N.addrProtocol addr))
      N.sClose  -- only done if there's an error
      (\sock -> sock <$ N.connect sock (N.addrAddress addr))

ignoreIOException :: IO () -> IO ()
ignoreIOException a = void (try a :: IO (Either IOException ()))

invalidStrategyError :: String -> IO a
invalidStrategyError name = putStrLn ("Error: No strategy `" ++ name ++ "' exists.") >> exitFailure

noStrategyError :: IO a
noStrategyError = putStrLn "Error: No strategy available." >> exitFailure

resolveStrategyName :: Maybe String -> [(String, a)] -> IO String
resolveStrategyName Nothing (x:_) = return $ fst x
resolveStrategyName Nothing [] = noStrategyError
resolveStrategyName (Just name) strategies = name <$ do
  unless (elem name . map fst $ strategies) $ invalidStrategyError name
  putStrLn $ "Using strategy `" ++ name ++ "'."


defaultMain :: (Serialize init, Storable move, NFData move, Storable state)
            => GameDefinition init move state
            -> [(String, StrategyCreator init move state)] -> IO ()
defaultMain game strategies = withSocketsDo $ do
  -- We don't want stdout to buffer more than a line, because our status messages are most often
  -- only a line long. But we also want some buffering, so that stdout is not *too* slow.
  -- When stdout is redirected to a file, we would get block-buffered if we didn't have this line.
  -- We need to do this before `handleForked`, because the forked process also writes the strategy
  -- statistics to stdout and thus also needs to be only line buffered. 
  hSetBuffering stdout LineBuffering

  -- This handles the case that we're in the re-executed version of ourselves that
  -- actually runs the strategy. See Client.Run for the details.
  -- If that is the case, handleForked never returns, otherwise it does nothing.
  handleForked game strategies

  -- If we're not in the strategy runner process (which we aren't at this point),
  -- we want to have the highest possible priority to minimize delay introduced
  -- through the scheduler.
  liftIO $ do
    changed <- setHighPriority
    when changed $ putStrLn "Adjusted process priority."

  -- Parse the command arguments and select the requested strategy or the default.
  args <- execParser clientArgs
  
  -- Monitoring
  liftIO $ do
    when (ekg args) $ do
      void $ forkServer "localhost" 7666
      putStrLn "EKG monitoring [client-runner] started on http://localhost:7666"

  realStrategy <- resolveStrategyName (strategy args) strategies

  -- Now connect to the server
  putStrLn $ "Connecting to server " ++ host args ++ ":" ++ show (port args) ++ " ..."
  es <- try $ connect (host args) $ port args
  case es of
    Left e | isDoesNotExistError e -> putStrLn "Couldn't connect to the game server. Is the server running?"
    Left e -> throwIO e
    Right s ->  flip finally (close s) $ do
      -- These socket options ought to reduce the delay, but it's not critical if they're not available,
      -- so we ignore errors from setting those.
      ignoreIOException $ setSocketOption s Cork 0 >> putStrLn "Successfully activated Cork socket option."
      ignoreIOException $ setSocketOption s NoDelay 1 >> putStrLn "Successfully activated NoDelay socket option."

      -- Unwrap the ExceptT of the client.
      -- In the case of an error (Left), we print it using `showError`. When we exit successfully (Right), we
      -- print the result to stdout.
      let handleResult = errorT (liftIO . putStrLn)
          process = handleResult $
            client game realStrategy (reservation args) (strict args) >>=
            liftIO . printResult
          handleDebug = if debug args then ($= conduitHandle stderr) else id

      -- Now finally, run the client.
      runResourceT $ handleDebug (sourceSocket s)
        $= parseBytes def
        $= process
        $$ renderBuilder def
        =$ CL.map toByteString
        =$ sinkSocket s

  where printResult gameResult = case winner gameResult of
          Nothing -> T.putStrLn $ "Tie!\n" <> reason gameResult
          Just p  -> T.putStrLn $ "Player " <> showPlayerColor p <> " won!\n" <> reason gameResult
