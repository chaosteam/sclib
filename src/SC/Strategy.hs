module SC.Strategy
  ( Strategy(..)
  , nullStrategy
  , StrategyCreator
  ) where

import SC.Types

data Strategy move state = Response
  { handleEnemy  :: move -> Strategy move state
  , updateState  :: state -> Strategy move state
  , moves        :: [(String, move, Strategy move state)]
  }

nullStrategy :: Strategy a b
nullStrategy = Response (\_ -> nullStrategy) (\_ -> nullStrategy) []

type StrategyCreator init move state = GameConfiguration -> state -> init -> Strategy move state
