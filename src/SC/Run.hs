{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
module SC.Run
  ( StrategyProcess()
  , createStrategy
  , handleForked
  , killStrategy
  , computeStrategy
  , stopStrategy
  , getStrategyResult
  , tellEnemyMove
  , tellGameState
  ) where

import Control.Applicative
import Control.Concurrent.Async
import Control.DeepSeq
import Control.Exception
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Data.IORef
import Data.Word
import Foreign.Ptr
import Foreign.Storable
import GHC.Generics (Generic)
import GHC.Stats
import Numeric.Lens
import Prelude
import System.Environment
import System.Exit
import System.IO
import System.Mem
import System.Process
import System.Remote.Monitoring

import qualified Data.ByteString as BS
import qualified Data.Serialize  as S

import SC.GameDefinition
import SC.Strategy
import SC.Types
import System.IPC.Util

import qualified System.IPC.Manager as Manager
import qualified System.IPC.Semaphore as Semaphore
import qualified System.IPC.SharedMem as SharedMem


--------------------------------------------------------------------------------
data Shared m s = Shared
  { currentBestMove  :: SharedMem.SharedMem (StorableMaybe m)
  , currentGameState :: SharedMem.SharedMem s
  , lastEnemyMove    :: SharedMem.SharedMem m
  , mayRun           :: Semaphore.Semaphore
  }

newtype StorableMaybe a = StorableMaybe (Maybe a)
instance Storable a => Storable (StorableMaybe a) where
  sizeOf _ = sizeOf (undefined :: a) + 1
  alignment _ = alignment (undefined :: a)
  peek ptr = StorableMaybe <$> do
    flag <- peek (ptr `plusPtr` sizeOf (undefined :: a))
    if flag == (0 :: Word8)
      then return Nothing
      else Just <$> peek (castPtr ptr)
  poke ptr (StorableMaybe Nothing) = poke (ptr `plusPtr` sizeOf (undefined :: a)) (0 :: Word8)
  poke ptr (StorableMaybe (Just a)) = do
    poke (castPtr ptr) a
    poke (ptr `plusPtr` sizeOf (undefined :: a)) (1 :: Word8)

data StrategyProcess m s = StrategyProcess
  { shared        :: Shared m s
  , commandHandle :: Handle
  , processHandle :: ProcessHandle
  }

sharedManager :: (Storable s, Storable m) => Manager.Manager' (Shared m s)
sharedManager = Shared
  <$> lmap currentBestMove (Manager.sharedMemory "currentBestMove")
  <*> lmap currentGameState (Manager.sharedMemory "currentGameState")
  <*> lmap lastEnemyMove (Manager.sharedMemory "lastEnemyMove")
  <*> lmap mayRun (Manager.semaphore "mayRun" 0)

createStrategyProcess :: (Storable s, Storable m) => s -> IO (StrategyProcess m s)
createStrategyProcess gs = do
  (sha, names) <- Manager.create sharedManager
  poke (SharedMem.pointer $ currentGameState sha) gs
  exe <- getInstanceExecutable
  (Just stdinH, _, _, pH) <- createProcess (proc exe ("fork":names))
    { std_in = CreatePipe
    }
  return $ StrategyProcess sha stdinH pH

killStrategy :: (Storable s, Storable m) => StrategyProcess m s -> IO ()
killStrategy StrategyProcess{..} = do
  terminateProcess processHandle
  void $ waitForProcess processHandle
  Manager.close sharedManager shared
  Manager.unlink sharedManager shared

--------------------------------------------------------------------------------
data Command = Compute | Stop | HandleEnemy | UpdateState deriving Enum

enumChar :: Enum a => Iso' a Char
enumChar = from enum.adding (fromEnum 'a').enum

getCommand :: IO Command
getCommand = getChar <&> review enumChar

sendCommand :: Handle -> Command -> IO ()
sendCommand h c = hPutChar h (c^.enumChar) >> hFlush h

--------------------------------------------------------------------------------
data RunnerInfo init = RunnerInfo
  { strategyName      :: String
  , gameConfiguration :: GameConfiguration
  , initState         :: init
  } deriving Generic
instance S.Serialize init => S.Serialize (RunnerInfo init)

startEvaluator :: Semaphore.Semaphore -> (Semaphore.Semaphore -> IO ()) -> StateT (Maybe (Async ())) IO ()
startEvaluator mayRun action = put . Just <=< liftIO $ do
  Semaphore.setToOne mayRun
  asyncWithUnmask $ \unmask -> handle handler $ unmask $ action mayRun
 where
  handler e = case fromException e of
    Just ThreadKilled -> return ()
    _ -> putStrLn $ "## EXCEPTION ## " ++ show e

stopEvaluator :: StateT (Maybe (Async ())) IO ()
stopEvaluator = get >>= traverse (liftIO . cancel) >> put Nothing

evaluator:: (Storable move, NFData move) => Ptr (StorableMaybe move) -> IORef (Strategy move state) -> Semaphore.Semaphore -> IO ()
evaluator currentbestmove strategyReference sem = do
  strategy <- readIORef strategyReference
  forM_ (moves strategy) $ \(message, value, strategy') -> do
    void $ evaluate $ force value
    putStrLn $ "[STATISTIC] " ++ message
    continue <- mask_ $ do
      continue <- Semaphore.withTry sem $ \c ->
        c <$ when c (poke currentbestmove . StorableMaybe . Just $ value)
      when continue $ writeIORef strategyReference strategy'
      return continue
    unless continue $ throwIO ThreadKilled

-- | Main entry point for the forked strategy process.
strategyProcessMain :: (S.Serialize init, Storable state, Storable move, NFData move)
                    => GameDefinition init move state              -- ^ the game to play
                    -> [(String, StrategyCreator init move state)] -- ^ available strategies
                    -> [String] -- ^ names of the shared resources (like shared memory)
                    -> IO ()
strategyProcessMain GameDefinition{..} strategies names = do
  statsEnabled <- getGCStatsEnabled
  when statsEnabled $ do
    void $ forkServer "localhost" 7665
    putStrLn "EKG monitoring [client-worker] started on http://localhost:7665"
  sha@Shared{..} <- Manager.open sharedManager names
  Manager.unlink sharedManager sha

  hSetBinaryMode stdin True
  len <- readLn
  Right (RunnerInfo{..}) <- S.decode <$> BS.hGet stdin len
  let Just f = lookup strategyName strategies
  gs <- peek $ SharedMem.pointer currentGameState
  strategyReference <- newIORef $ f gameConfiguration gs initState
  flip evalStateT Nothing $ forever $ do
    command <- liftIO getCommand
    stopEvaluator
    case command of
      Stop    -> liftIO $ do
        performGC
        when statsEnabled $ do
          stats <- getGCStats
          putStrLn $ "[GC STATISTIC] " ++ showStats stats
      Compute -> startEvaluator mayRun $ evaluator (SharedMem.pointer currentBestMove) strategyReference
      UpdateState -> liftIO $ do
        gamestate' <- peek (SharedMem.pointer currentGameState)
        modifyIORef strategyReference $ flip updateState gamestate'
      HandleEnemy -> liftIO $ do
        move       <- peek (SharedMem.pointer lastEnemyMove)
        modifyIORef strategyReference $ flip handleEnemy move

showStats :: GCStats -> String
showStats GCStats{..} = unwords $
  ["alloc:" ++ show bytesAllocated
  ,"gcs:" ++ show numGcs
  ,"max:" ++ show maxBytesUsed
  ,"max_alloc:" ++ show peakMegabytesAllocated ++ "M"
  ,"gc_time:" ++ show gcCpuSeconds ++ "s"
  ,"productivity:" ++ show (mutatorCpuSeconds / cpuSeconds * 100) ++ "%"
  ]

-- | Call this function in main to handle the switch between the strategy process runner
-- and the normal client.
handleForked :: (S.Serialize init, Storable state, Storable move, NFData move) => GameDefinition init move state -> [(String, StrategyCreator init move state)] -> IO ()
handleForked game strategies = do
  args <- getArgs
  case args of
    ("fork":args') -> strategyProcessMain game strategies args' >> exitSuccess
    _ -> return ()

createStrategy :: (S.Serialize i, Storable m, Storable s) => String -> GameConfiguration -> s -> i -> IO (StrategyProcess m s)
createStrategy name gc gs i = do
  strat <- createStrategyProcess gs
  poke (SharedMem.pointer . currentBestMove . shared $ strat) $ StorableMaybe Nothing
  hSetBinaryMode (commandHandle strat) True
  let enc = S.encode (RunnerInfo name gc i)
  hPrint (commandHandle strat) (BS.length enc)
  BS.hPut (commandHandle strat) enc
  hFlush $ commandHandle strat
  return strat

stopStrategy :: StrategyProcess m s -> IO ()
stopStrategy StrategyProcess{commandHandle} = sendCommand commandHandle Stop

computeStrategy :: Storable m => StrategyProcess m s
                -> IO ()
computeStrategy StrategyProcess{ commandHandle } = sendCommand commandHandle Compute

getStrategyResult :: Storable m => StrategyProcess m s -> IO (Maybe m)
getStrategyResult StrategyProcess { shared = Shared{currentBestMove, mayRun} } = do
  Semaphore.wait mayRun >> Semaphore.lock mayRun
  StorableMaybe m <- peek (SharedMem.pointer currentBestMove)
  poke (SharedMem.pointer currentBestMove) $ StorableMaybe Nothing
  return m

tellGameState :: Storable s => StrategyProcess m s
              -> s -- ^ The new game state
              -> IO ()
tellGameState StrategyProcess { shared = Shared{..}, commandHandle } gamestate = do
  poke (SharedMem.pointer currentGameState) gamestate
  sendCommand commandHandle UpdateState

tellEnemyMove :: Storable m => StrategyProcess m s
              -> m -- ^ Move of the enemy
              -> IO ()
tellEnemyMove StrategyProcess{ shared = Shared{..}, commandHandle } move = do
  poke (SharedMem.pointer lastEnemyMove) move
  sendCommand commandHandle HandleEnemy
