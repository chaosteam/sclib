{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE CPP #-}
module System.IPC.Util
  ( findAvailableName
  , getInstanceExecutable
  , setHighPriority
#ifndef mingw32_HOST_OS
  , oCreat, oRdwr, oExcl
  , sIrusr, sIwusr
#endif
  ) where

import Control.Exception hiding (handle)

#ifdef mingw32_HOST_OS
import System.Win32.DLL
import System.Win32.Types
#else
import Foreign.C.Types
import System.Posix.Files
#endif

-- | Run an action that takes a name and may throw an IO exception. If
-- it doesn't throw an IO exception, return the result. Else, retry
-- with a slightly different name.
findAvailableName :: forall a. String -> (String -> IO a) -> IO a
findAvailableName initial f = go Nothing where
  go :: Maybe Int -> IO a
  go i = do
    r <- try $ f $ initial ++ maybe "" show i
    case r of
      Left (_ :: IOException) -> go $ Just $ maybe 2 (+1) i
      Right a                -> return a

-- | Get the path of the executable of the current process instance.
getInstanceExecutable :: IO String

-- | This changes the priority of the current process to 'high'. On Linux, this
-- doesn't seem to be required and is a no-op. This function returns false when
-- the priority couldn't be changed.
setHighPriority :: IO Bool

#ifdef mingw32_HOST_OS
--------------------------------------------------------------------------------
-- Windows implementation
#include <windows.h>

getInstanceExecutable = getModuleFileName nullPtr

setHighPriority = do
  handle <- c_GetCurrentProcess
  -- If HIGH_PRIORITY_CLASS is not allowed because of access level, maybe above normal priority
  -- class is allowed. I don't know if that is actually possible on Windows, but let's just be safe.
  a <- c_SetPriorityClass handle aBOVE_NORMAL_PRIORITY_CLASS
  b <- c_SetPriorityClass handle hIGH_PRIORITY_CLASS
  return $ a || b

##if defined(i386_HOST_ARCH)
## define WINDOWS_CCONV stdcall
##elif defined(x86_64_HOST_ARCH)
## define WINDOWS_CCONV ccall
##else
## error Unknown mingw32 arch
##endif

hIGH_PRIORITY_CLASS :: DWORD
hIGH_PRIORITY_CLASS = #const HIGH_PRIORITY_CLASS

aBOVE_NORMAL_PRIORITY_CLASS :: DWORD
aBOVE_NORMAL_PRIORITY_CLASS = #const ABOVE_NORMAL_PRIORITY_CLASS

foreign import WINDOWS_CCONV safe "windows.h SetPriorityClass"
  c_SetPriorityClass :: HANDLE -> DWORD -> IO BOOL

foreign import WINDOWS_CCONV safe "windows.h GetCurrentProcess"
  c_GetCurrentProcess :: IO HANDLE

#else
--------------------------------------------------------------------------------
-- Posix implementation

getInstanceExecutable = readSymbolicLink "/proc/self/exe"

setHighPriority = return False

#include <fcntl.h>
#include <sys/stat.h>

oCreat, oRdwr, oExcl :: CInt
oCreat = #const O_CREAT
oRdwr = #const O_RDWR
oExcl = #const O_EXCL

sIrusr, sIwusr :: CInt
sIrusr = #const S_IRUSR
sIwusr = #const S_IWUSR
#endif
