{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ScopedTypeVariables #-}
module System.IPC.Manager
  ( Manager()
  , Manager'
  , create
  , open
  , close
  , unlink
  , sharedMemory
  , semaphore
  ) where

import Control.Applicative
import Control.Lens
import Control.Monad.Catch
import Control.Monad.Trans.Class
import Control.Monad.Trans.State
import Control.Monad.Trans.Writer
import Data.Monoid
import Foreign.Storable

import qualified System.IPC.Semaphore as Semaphore
import qualified System.IPC.SharedMem as SharedMem

-- | 'Manager' allows to manage complex IPC structures. Without @Manager@, you need to
-- carefully ensure manually that every open is paired with a close, etc. The @Manager@
-- helps with opening / closing / unlinking / ... larger structures that are composed
-- of the simple IPC primitives as a whole.
--
-- @r@ is the type that all unlink / close functions take as an argument.
-- @a@ is the type that all create / open functions return as a result.
--
-- Those types should not be different for top-level allocators, but having
-- them separate allows us to have intermediate values were those types are
-- different and to make a @Profunctor@ instance.
--
-- As an user, you should almost never need to use this type directly. Use @Manager'@ instead.
data Manager r a = Manager
  { managerCreate   :: WriterT (Endo [String]) IO (a, IO ())
  , managerOpen     :: StateT [String] IO (a, IO ())
  , managerClose    :: r -> IO ()
  , managerUnlink   :: r -> IO ()
  } deriving Functor
-- Invariant: open/create are atomic in the sense that if they throw an exception, all operations will
-- be rolled back.

-- | A manager for values of type 'a'. This is just a @Manager@ where both type arguments are the same.
type Manager' a = Manager a a

instance Applicative (Manager r) where
  pure x = Manager (pure (x, ignore)) (pure (x, ignore)) (const ignore) (const ignore) where
    ignore :: Applicative f => f ()
    ignore = pure ()
  Manager n o c u <*> Manager n' o' c' u' = Manager (n >+> n') (o >+> o') (c >-> c') (u >-> u') where
      (>->) = liftA2 finally
      (>+>) :: (MonadTrans t, MonadCatch (t IO)) => t IO (a -> b, IO ()) -> t IO (a, IO ()) -> t IO (b, IO ())
      a >+> b = do
        -- The following code ensures that we also free all resources that we already allocated
        -- in the case that the allocating function throws an exception.
        --
        -- To achieve that, each allocation action also returns an action that reverses the effects
        -- of the allocation. We just run this "rollback" function when we need to reverse the effects
        -- because we encountered an exception later on.
        (a', rollbackA) <- a
        (b', rollbackB) <- b `onException` lift rollbackA
        return (a' b', rollbackA `finally` rollbackB)

instance Profunctor Manager where
  rmap = fmap
  lmap f (Manager n o c u) = Manager n o (c . f) (u . f)
  dimap f g (Manager n o c u) = Manager (fmap (over _1 g) n) (fmap (over _1 g) o) (c . f) (u . f)

-- | Create all resources registered with a manager.
create :: Manager x o -> IO (o, [String])
create = fmap (bimap fst (`appEndo` [])) . runWriterT . managerCreate

-- | Open all resources registered with a manager.
open :: Manager x o -> [String] -> IO o
open man names = fst <$> evalStateT (managerOpen man) names

-- | Close all resources registered with a manager.
close :: Manager i x -> i -> IO ()
close = managerClose

-- | Unlink all resources registered with a manager.
unlink :: Manager i x -> i -> IO ()
unlink = managerUnlink

-- | Construct a new manager from a typical create, open, close and unlink function.
manager :: forall a. IO a       -- ^ Create function
        -> (String -> IO a) -- ^ Open function
        -> (a      -> IO ()) -- ^ Close function
        -> (a      -> IO ()) -- ^ Unlink function
        -> (a -> String)    -- ^ Function to extract resource name
        -> Manager' a
manager n o c u name = Manager
  (fmap makeRollbackCreate $ lift n >>= pushName)
  (fmap makeRollbackOpen $ popName >>= lift . o)
  c
  u
 where
  popName :: (Functor m, Monad m) => StateT [b] m b
  popName = get >>= \(x:xs) -> x <$ put xs

  pushName :: (Functor m, Monad m) => a -> WriterT (Endo [String]) m a
  pushName x = x <$ tell (Endo (name x:))

  makeRollbackCreate :: a -> (a, IO ())
  makeRollbackCreate r = (r, u r `finally` c r)

  makeRollbackOpen :: a -> (a, IO ())
  makeRollbackOpen r = (r, c r)

-- | Manage a shared memory resource with the given name hint and size.
sharedMemory :: Storable a => String -> Manager' (SharedMem.SharedMem a)
sharedMemory name = manager (SharedMem.create name) SharedMem.open SharedMem.close SharedMem.unlink SharedMem.name

-- | Manager a semaphore with the given name hint and initial value.
semaphore :: String -> Int -> Manager' Semaphore.Semaphore
semaphore name value = manager (Semaphore.create name value) Semaphore.open Semaphore.close Semaphore.unlink Semaphore.name
