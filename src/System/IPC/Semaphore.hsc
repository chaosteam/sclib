{-# LANGUAGE InterruptibleFFI #-}
{-# LANGUAGE CPP #-}
module System.IPC.Semaphore
  ( Semaphore()
  , name
  , create
  , open
  , close
  , unlink
  , wait
  , tryWait
  , threadWait
  , post
  , with
  , withTry
  , lock
  , setToOne
  ) where

import Control.Concurrent
import Control.Exception (finally)
import Control.Monad
import Control.Monad.Loops
import Data.Bits
import Foreign.C.Types
import Foreign.Ptr
import System.IPC.Util

#ifdef mingw32_HOST_OS
--------------------------------------------------------------------------------
-- Windows imports
import System.Win32.Types
import System.Win32.File
import System.Win32.Process (iNFINITE)

#else
----------------------------------------------------------------------
-- Posix imports
import Foreign.C.Error
import Foreign.C.String

#endif

-- | Create a new semaphore, with the given initial value.
--
-- The @String@ argument is a hint for the name to use for the semaphore.
-- This function will automatically find a name that is not yet used, based on the hint.
create :: String -> Int -> IO Semaphore

-- | Open the semaphore with the given name. Throw an exception if no semaphore
-- with that name exists.
open :: String -> IO Semaphore

-- | Close the semaphore. This closes the handle to the semaphore, but doesn't necessarily unlink
-- the semaphore itself. On Windows, the semaphore will be unlinked if the last reference to
-- it is closed.
close :: Semaphore -> IO ()

-- | Unlink a semaphore. This removes the semaphore, so the semaphore isn't accessible by the
-- name after calling this function. As the handle is not closed, the semaphore can still be used.
unlink :: Semaphore -> IO ()

-- | Decrement the semaphore. If the current value of the semaphore is 0, block until
-- it becomes greater than zero.
wait :: Semaphore -> IO ()

-- | Decrement the semaphore if the current value is greater than zero and return True.
-- Otherwise, return False.
tryWait :: Semaphore -> IO Bool

-- | Poll the semaphore until it is available. Unlink @wait@, this will block only the current
-- thread rather than the entire process.
threadWait :: Semaphore -> IO ()
threadWait sem = do
  isFree <- tryWait sem
  unless isFree $ yield >> threadWait sem

-- | Increment the semaphore count.
post :: Semaphore -> IO ()

-- | Decrement the semaphore, perform some action and increment it again. This uses
-- @threadWait@ such that it blocks only the current thread.
with :: Semaphore -> IO a -> IO a
with sem a = threadWait sem >> a `finally` post sem

-- | Try to decrement the semaphore, then run the given action passing a bool indicating
-- whether the decrement was successful or not. If the decrement was successful, incremen the semaphore again after performing the action.
withTry :: Semaphore -> (Bool -> IO a) -> IO a
withTry sem f = do
  isFree <- tryWait sem
  f isFree `finally` when isFree (post sem)

-- | Lock a semaphore by decrementing until it's value is zero.
-- Note that this doesn't block if the semaphore value is already zero.
lock :: Semaphore -> IO ()
lock = void . iterateWhile id . tryWait

-- | Decrement the semaphore until it has value 1.
setToOne :: Semaphore -> IO ()
setToOne sem = lock sem >> post sem

#ifdef mingw32_HOST_OS
--------------------------------------------------------------------------------
-- Windows implementation

#include <windows.h>

##if defined(i386_HOST_ARCH)
## define WINDOWS_CCONV stdcall
##elif defined(x86_64_HOST_ARCH)
## define WINDOWS_CCONV ccall
##else
## error Unknown mingw32 arch
##endif

data Semaphore = Semaphore
  { name :: String
  , handle :: HANDLE
  }

create hint value = findAvailableName hint $ \n -> withTString n $ \cn -> do
  h <- c_CreateSemaphore nullPtr (fromIntegral value) maxBound cn
  err <- getLastError
  unless (err == 0) $ errorWin "Semaphore.create"
  return $ Semaphore n h

open n = fmap (Semaphore n) . failIfNull "Semaphore.open" $ withTString n $
  c_OpenSemaphore (sEMAPHORE_MODIFY_STATE .|. sYNCHRONIZE) False

close = void . c_CloseHandle . handle

unlink _ = return ()

post sem = failIfFalse_ "Semaphore.post" $ c_ReleaseSemaphore (handle sem) 1 nullPtr

wait sem = failIf_ (/= 0) "Semaphore.wait" $ c_WaitForSingleObject (handle sem) iNFINITE

tryWait sem = do
  r <- failIf (== wAIT_FAILED) "Semaphore.tryWait" $ c_WaitForSingleObject (handle sem) 0
  return $ r /= wAIT_TIMEOUT

sEMAPHORE_MODIFY_STATE :: DWORD
sEMAPHORE_MODIFY_STATE = #const SEMAPHORE_MODIFY_STATE

wAIT_TIMEOUT :: DWORD
wAIT_TIMEOUT = #const WAIT_TIMEOUT

wAIT_FAILED :: DWORD
wAIT_FAILED = #const WAIT_FAILED

foreign import WINDOWS_CCONV safe "windows.h CreateSemaphoreW"
  c_CreateSemaphore :: Ptr () -> LONG -> LONG -> LPCTSTR -> IO HANDLE

foreign import WINDOWS_CCONV safe "windows.h OpenSemaphoreW"
  c_OpenSemaphore :: DWORD -> BOOL -> LPCTSTR -> IO HANDLE

foreign import WINDOWS_CCONV safe "windows.h ReleaseSemaphore"
  c_ReleaseSemaphore :: HANDLE -> LONG -> Ptr LONG -> IO BOOL

foreign import WINDOWS_CCONV interruptible "windows.h WaitForSingleObject"
  c_WaitForSingleObject :: HANDLE -> DWORD -> IO DWORD

#else
--------------------------------------------------------------------------------
-- Posix implementation

data Semaphore = Semaphore
  { name :: String
  , ptr  :: Ptr ()
  }

create hint value = findAvailableName hint $ \n -> semOpenWith n (oCreat .|. oExcl) (sIrusr .|. sIwusr) value

open n = semOpenWith n 0 0 0

semOpenWith :: String -> CInt -> CInt -> Int -> IO Semaphore
semOpenWith n flags mode value = fmap (Semaphore n) $ withCString n $ \cname ->
  throwErrnoIfNull "Semaphore.semOpenWith" $ sem_open cname flags mode (fromIntegral value)

close = void . sem_close . ptr

unlink s = void $ withCString (name s) sem_unlink

post = void . sem_post . ptr

wait = throwErrnoIfMinus1Retry_ "Semaphore.wait" . sem_wait . ptr

tryWait s = go where
  go :: IO Bool
  go = do
    res <- sem_trywait (ptr s)
    if res == 0
      then return True
      else do
        errno <- getErrno
        case errno of
          e | e == eAGAIN -> return False
            | e == eINTR  -> go
            | otherwise  -> throwErrno "Semaphore.tryWait"

foreign import ccall safe "sem_open"
        sem_open :: CString -> CInt -> CInt -> CUInt -> IO (Ptr ())
foreign import ccall safe "sem_close"
        sem_close :: Ptr () -> IO CInt
foreign import ccall safe "sem_unlink"
        sem_unlink :: CString -> IO CInt
foreign import ccall safe "sem_post"
        sem_post :: Ptr () -> IO CInt
foreign import ccall interruptible "sem_wait"
        sem_wait :: Ptr () -> IO CInt
foreign import ccall safe "sem_trywait"
        sem_trywait :: Ptr () -> IO CInt

#endif
