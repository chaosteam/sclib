{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ForeignFunctionInterface #-}
module System.IPC.SharedMem
  ( SharedMem()
  , name
  , pointer
  , create
  , open
  , close
  , unlink
  ) where

import Control.Monad
import Data.Bits
import Foreign.Ptr
import Foreign.Storable
import System.IPC.Util

#ifdef mingw32_HOST_OS
--------------------------------------------------------------------------------
-- Windows imports
import System.Win32.FileMapping
import System.Win32.File
import System.Win32.Types
import System.Win32.Mem

#else
----------------------------------------------------------------------
-- Posix imports
import Control.Applicative
import Data.Maybe
import Foreign.C.Error
import Foreign.C.String
import Foreign.C.Types
import System.Posix.Files
import Prelude
#endif

-- | Allocate a new named shared memory object and open it for reading and writing.
--
-- This function will use the given name as a hint and will try to use a name similar
-- to the hint for the shared memory object.
create :: forall a. Storable a => String -> IO (SharedMem a)

-- | Open an existing named shared memory object. Throw an exception if no such
-- shared memory object with the given name exists.
open :: forall a. Storable a => String -> IO (SharedMem a)

-- | Unmap a shared memory object. This function only closes the handle to the shared memory
-- object, but doesn't unlink the shared memory object itself.
close :: forall a. Storable a => SharedMem a -> IO ()

-- | Unlink the shared memory object, but do not unmap it.
unlink :: SharedMem a -> IO ()

#ifdef mingw32_HOST_OS
--------------------------------------------------------------------------------
-- Windows implementation

data SharedMem a = SharedMem
  { name    :: String
  , pointer :: Ptr a
  , handle  :: HANDLE
  }

create hint = do
  (n, h) <- findAvailableName hint $ \n -> do
    h <- createFileMapping Nothing (pAGE_READWRITE) (fromIntegral $ sizeOf (undefined :: a)) (Just n)
    err <- getLastError
    unless (err == 0) $ errorWin "SharedMem.create"
    return (n, h)
  ptr <- mapViewOfFile h access 0 (fromIntegral $ sizeOf (undefined :: a))
  return $ SharedMem n ptr h
 where access = (fILE_MAP_READ .|. fILE_MAP_WRITE)

open n = do
  h   <- failIfNull "SharedMem.open" $ openFileMapping access False (Just n)
  ptr <- failIfNull "SharedMem.open" $ mapViewOfFile h access 0 (fromIntegral $ sizeOf (undefined :: a))
  return $ SharedMem n ptr h
 where access = (fILE_MAP_READ .|. fILE_MAP_WRITE)

close shm = do
  void $ c_UnmapViewOfFile (pointer shm)
  void $ c_CloseHandle (handle shm)

unlink _ = return ()

#else
--------------------------------------------------------------------------------
-- Posix implementation
#include <sys/mman.h>

data SharedMem a = SharedMem
  { name    :: String
  , pointer :: Ptr a
  }

mapFd :: CInt -> CSize -> IO (Ptr a)
mapFd fd s = do
  ptr <- mmap nullPtr s (protRead .|. protWrite) mapShared fd 0
  when (ptr == nullPtr `plusPtr` (-1)) $ throwErrno "SharedMem.mapFd"
  setFdSize (fromIntegral fd) $ fromIntegral s
  return $ castPtr ptr

create n = findAvailableName n' $ \cn -> shmOpenWith (oRdwr .|. oCreat .|. oExcl) cn where
  n' :: String
  n' | listToMaybe n == Just '/' = n
     | otherwise                = '/' : n

open = shmOpenWith oRdwr

shmOpenWith :: forall a. Storable a => CInt -> String  -> IO (SharedMem a)
shmOpenWith opt n = withCString n $ \cname -> do
  fd <- throwErrnoIfMinus1 "SharedMem.shmOpenWith" $ shm_open cname opt (sIrusr .|. sIwusr)
  SharedMem n <$> mapFd fd (fromIntegral size)
 where
  size :: Int
  size = sizeOf (undefined :: a)

close shm = void $ munmap (castPtr $ pointer shm) $ fromIntegral (sizeOf (undefined :: a))

unlink shm = void $ withCString (name shm) shm_unlink

protRead, protWrite :: CInt
protRead = #const PROT_READ
protWrite = #const PROT_WRITE

mapShared :: CInt
mapShared = #const MAP_SHARED

foreign import ccall safe "mmap"
  mmap :: Ptr () -> CSize -> CInt -> CInt -> CInt -> CInt -> IO (Ptr ())

foreign import ccall safe "munmap"
  munmap :: Ptr () -> CSize -> IO CInt

foreign import ccall safe "shm_open"
  shm_open :: CString -> CInt -> CInt -> IO CInt

foreign import ccall safe "shm_unlink"
  shm_unlink :: CString -> IO CInt

#endif
